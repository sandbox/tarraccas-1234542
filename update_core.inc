<?php
// $Id$
/**
 * @file
 * Admin functions.
 */

/**
 * Update Core Configuration Form
 */
function update_core_form_admin() {

	$form = array();

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Update Drupal Core')
	);

	$form['options'] = array(
		'#type' => 'fieldset',
		'#title' => t('Options'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'force' => array(
			'#type' => 'checkbox',
			'#title' => t('Force Update'),
			'#description' => t('Update Drupal installation, even if <b>!version</b> is the latest version. This may be necessary if an error happens in a prior update attempt.', array('!version' => VERSION))
		),
		'update_core_chmodfiles' => array(
			'#type' => 'textarea',
			'#title' => t('Make Writable'),
			'#description' => t('Make the these files and directories (relative to the Drupal root, one per line, ie. "<code>sites/default</code>") writable prior to update. Updating read-only files or directories will fail and throw an exception. Don\'t add to this list unless exceptions occur during update.'),
			'#default_value' => implode("\n", variable_get('update_core_chmodfiles', array()))
		)
	);

	return $form;

}

/**
 * Update Core Configuration Form Submit
 */
function update_core_form_admin_submit($form, &$form_state) {

	// save chmod files ...
	$files = explode("\n", $form_state['values']['update_core_chmodfiles']);
	$files = array_map('trim', $files);	// trim all values
	$files = array_filter($files); // remove empty values
	variable_set('update_core_chmodfiles', $files);

	// http context ...
	$context = stream_context_create(array('http' => array(
		'method' => 'GET',
		'header' => implode("\n\n", array(
			'User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
			'Referer: ' . $url
		))
	)));

	// url to check for updates on ...
	$url = 'https://www.drupal.org/node/3060/release?api_version[]=103';

	// read the Drupal project page ... http://drupal.org/project/drupal
	$page = file_get_contents($url, FALSE, $context);

	// parse the archive urls ...
	$url = FALSE;
	$subversion = preg_replace('/^\d{1,}\.(\d{1,})/', "$1", VERSION);
	if (preg_match_all(sprintf('/(https:.*?drupal-%d\.(\d{1,})\.tar\.gz)/', intval(VERSION)), $page, $matches, PREG_SET_ORDER)) {
		foreach (array_reverse($matches) as $match) {
			// if the archive version is greater than current core version then set url
			if ($form_state['values']['force'] == 1 ? $match[2] >= $subversion : $match[2] > $subversion) {
				$url = $match[1];
				$subversion = $match[2];
			}
		}
	}

	// an update is available ... continue ...
	if ($url) {

		// download to temp directory ...
		$archive_path = file_directory_temp() . '/' . pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_BASENAME);

		// name of the directory contained within the archive ...
		// call pathinfo twice to strip off the double .tar.gz extension ...
		$archive_name = pathinfo(pathinfo($archive_path, PATHINFO_FILENAME), PATHINFO_FILENAME);

		// download archive ...
		if (file_put_contents($archive_path, file_get_contents($url, FALSE, $context))) {

			// make following files/directories writable or else Archiver throws error ...
			$files = array_fill_keys($files, 0755);
			foreach ($files as $file => $oct) {
				// record previous file permissions ...
				//$oct = substr(sprintf('%o', fileperms($file)), -4);
				//$files[$file] = $oct;
				// temporarily update permissions ...
				chmod($file, $oct);
			}

			// access archive ...
			$A = new ArchiverTar($archive_path);
			$T = $A->getArchive();

			// suppress Drupal's exception handler ...
			//set_exception_handler('update_core_exception_handler');

			// extract files to drupal root sans leading archive name/dir contained in archive ...
			$T->extractModify(DRUPAL_ROOT, $archive_name . '/');

			// restore Drupal's exception handler ...
			//restore_exception_handler();

			// clean up ...
			unset($T, $A);
			unlink($archive_path);

			// return files to their previous permissions ...
			//foreach ($files as $file => $oct) chmod($file, $oct);

			// celebrate!
			drupal_set_message(t('Drupal successfully updated to version %version! You should probably run the update script now.', array('%version' => sprintf('%d.%d', VERSION, $subversion))));
		}

		// failure to download archive ...
		else {
			drupal_set_message(t("Unable to write file '%path'!", array('%path' => $path)), 'warning');
		}

	}

	// no updates found ...
	else {
		drupal_set_message(t('No new updates available currently.'));
	}

}

/**
 * Exception handler to temporarily suppress Drupal's catastrophic handler ... will keep going in spite of errors ...

function update_core_exception_handler($E) {
	drupal_set_message($E->getMessage(), 'warning');
}
 */
